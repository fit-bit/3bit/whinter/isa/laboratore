# ISA 2020: Odpovědní arch pro cvičení č. 1

## Zjišťování konfigurace

### (1.) Rozhraní enp0s3

*MAC adresa*: 08:00:27:7a:b6:82

*IPv4 adresa*: 10.0.2.15

*Délka prefixu*: 24

*Adresa síťe*: 10.0.2.0

*Broadcastová adresa*: 10.0.2.255

### (2.) Výchozí brána

*MAC adresa*: 52:54:00:12:35:02

*IPv4 adresa*: 10.0.2.2

### (4.) DNS servery

*Soubor*: /etc/resolv.conf

*DNS servery*: nameserver 10.0.2.3

### (5.) Ping na výchozí bránu

*Soubor*: /etc/hosts

*Úprava*: 10.0.2.2 gw

### (6.) TCP spojení

*Záznam + popis*:

State  Recv-Q   Send-Q   Local Address:Port      Peer Address:Port
ESTAB   0        0        10.0.2.15:40412         99.86.243.108:https

State - Stav TCP spojení
Revc-Q - Počet bytů které si uživatelský program nezkopíroval ze soketu.
Send-Q - Počet bytů které nepotvrdil vzdálený host.
Local Address:Port - Adresa a číslo portu místního soketu na kterém komunikuje nějaký proces.
Peer Address:Port - Adresa a číslo portu vzdáleného soketu (zařízení se kterým komunikujeme).

### (8.) NetworkManager události

*Příkaz*: sudo journalctl -u NetworkManager

### (9.) Chybová hláška sudo

*Příkaz*: sudo wireshark

*Chybová hláška*: Oct 01 09:10:14 localhost.localdomain sudo[2924]:  user : command not allowed ; TTY=pts/1 ; PWD=/home/user ; USER=root ; COMMAND=/bin/wireshark

## Wireshark

### (1.) Capture filter

*Capture filter*: port 80

### (2.) Zachycená HTTP komunikace

Komu patří nalezené IPv4 adresy a MAC adresy?: IP adresy patří klientovy (naše pc) a serveru se kterým komunikujeme. MAC adresy patří rozhraní na klientovy (naše pc) a rozhraní na výchozí bráně.
Vypisovali jste již některé z nich?: Ano naši IP a MAC adresu a MAC adresu výchozí brány.
Proč tomu tak je?: Klient komunikuje se serverem na internetu, takže používá svou IP a MAC adresu pro směrování, MAC adresa brány je zobrazena protom že se používá pro smětování na L2 vrstně. Aby paket mohl pokračovat dál do internetu, musí prvně projít přes výchozí bránu.

#### Požadavek HTTP

Cílová MAC adresa

  - *Adresa*: 52:54:00:12:35:02
  - *Role zařízení*: MAC adresa rozhraní na výchozí bráně, přes které jde zpráva dál do internetu

Cílová IP adresa

  - *Adresa*: 147.229.177.179
  - *Role zařízení*: IP adresa serveru kterému posíláme požadavek

Zdrojová MAC adresa

  - *Adresa*: 08:00:27:7a:b6:82
  - *Role zařízení*:  MAC adresa síťové karty klienta (naše pc), která určuje kam má přijít odpověď

Zdrojová IP adresa

  - *Adresa*: 10.0.2.15
  - *Role zařízení*: IP adresa klienta (naše pc), která určuje kam má přijít odpověď


#### Odpověď HTTP

Cílová MAC adresa

  - *Adresa*: 08:00:27:7a:b6:82
  - *Role zařízení*: MAC adresa síťové karty klienta

Cílová IP adresa

  - *Adresa*: 10.0.2.15
  - *Role zařízení*: IP adresa klienta

Zdrojová MAC adresa

  - *Adresa*: 52:54:00:12:35:02
  - *Role zařízení*: MAC adresa rozhraní na výchozí bráně, které přeposílá zprávu našemu počítači.

Zdrojová IP adresa

  - *Adresa*: 147.229.177.179
  - *Role zařízení*: IP adresa serveru který nášemu počítači (klientovy) posílá odpověď

### (3.) Zachycená ARP komunikace

*Display filter*: arp || icmp

### (6.) Follow TCP stream

Jaký je formát zobrazených dat funkcí *Follow TCP stream*, slovně popište
význam funkce *Follow TCP stream*: Funkce umožňuje přehledně v jednom bloku zobrazit celou TCP komunikaci našeho zařízení s jiným zařízením. Formát je tedy mnohem čitelnější než kdyby jsme to museli procházet ručně mezi pakety, všechna data jsou vypsána v jednou okně a rozlišena červenou a modrou barvou, aby bylo jasné co která která strana komunikovala.