# ISA 2020: Odpovědní arch pro cvičení č. 2

## Vzdálený terminál - SSH, Secure Shell

### (2.) Bezpečné připojení na vzdálený počítač bez autentizačních klíčů

*Verze OpenSSH*: 7.4

*Vybraná podporovaná šifra*: chacha20-poly1305

*Co lze zjistit o obsahu komunikace?*: Ne

### (3.) Vytvoření veřejného a privátního klíče

*Jak se liší práva mezi souborem s privátním a veřejným klíčem?*: U obou klíčů může uživatel číst a zapisovat. U veřejného klíče můžou navíc číst uživatelé ve skupině a všichni ostatní uživatelé.

### (4.) Distribuce klíčů

*Jaká hesla bylo nutné zadat při kopírovaní klíčů?*: User: user4lab     Root: root4lab

*Jaká hesla bylo nutné zadat při opětovném přihlášení?*: User: fitvutisa    Root: <žádné>

### (6.) Pohodlné opakované použití klíče

*Museli jste znovu zadávat heslo?*: Poprvé bylo potřeba zadat klíč, při dalších připojení už to nebylo třeba.

## Zabezpečení transportní vrstvy - TLS, Transport Layer Security

### (1.) Nezabezpečený přenos dat

*Je možné přečíst obsah komunikace?*: Ano

### (2.) Přenos dat zabezpečený TLS

*Je možné přečíst obsah komunikace?*: Ne
