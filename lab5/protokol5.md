## Laboratorní protokol cv. 5

Jméno: Matěj Kudera
Datum: 27.11.2020
Login: xkuder04

IP adresa rozhraní enp0s8 **cv5-master**: 192.168.56.102/24
IP adresa rozhraní enp0s8 **cv5-provider**: 192.168.56.101/24

### Úkol 1, Netflow
1. Uveďte TOP 10 IP adres podle počtu přenesených bytů:
    Top 20 IP Addr ordered by bytes:
    Date first seen          Duration Proto           IP Addr    Flows(%)     Packets(%)       Bytes(%)         pps      bps   bpp
    2016-06-26 01:08:02.008 4380711.968 any        136.2.58.138    2.3 M(18.0)  176.9 M(37.6)  159.1 G(38.7)       40   290599   899
    2016-06-26 08:05:48.984 4355635.896 any       185.2.119.236    1.1 M( 8.5)   52.7 M(11.2)   44.4 G(10.8)       12    81591   842
    2016-08-14 17:58:46.168 86237.112 any        136.2.24.237     8550( 0.1)   25.1 M( 5.3)   25.1 G( 6.1)      291    2.3 M  1000
    2016-06-26 19:06:04.984 4315942.296 any        136.2.24.236     8384( 0.1)   21.3 M( 4.5)   21.4 G( 5.2)        4    39646  1006
    2016-08-14 17:59:25.192 86362.736 any        136.2.24.239     8343( 0.1)   16.3 M( 3.5)   16.2 G( 4.0)      189    1.5 M   993
    2016-08-14 18:06:30.680 85506.600 any        136.2.24.238     6921( 0.1)   14.5 M( 3.1)   14.8 G( 3.6)      169    1.4 M  1018
    2016-06-26 18:16:18.008 4318988.896 any      122.153.220.92    70330( 0.5)   11.4 M( 2.4)   11.2 G( 2.7)        2    20658   980
    2016-08-15 06:20:05.168 41933.152 any       42.208.57.175      697( 0.0)    9.3 M( 2.0)    9.4 G( 2.3)      220    1.8 M  1010
    2016-08-15 00:20:23.256 63366.024 any     124.106.137.204     1696( 0.0)    8.1 M( 1.7)    9.4 G( 2.3)      127    1.2 M  1154
    2016-06-26 08:27:51.984 4325041.808 any       36.122.83.223   333178( 2.6)   13.4 M( 2.8)    9.2 G( 2.2)        3    17034   686

2. Uveďte 3 datové protokoly s nejvyšším objemem přenesených bytů
   Top 20 Protocol ordered by bytes:
    Date first seen          Duration Proto          Protocol    Flows(%)     Packets(%)       Bytes(%)         pps      bps   bpp
    2016-06-26 01:08:02.008 4380698.264 TCP                   6    5.2 M(40.8)  325.2 M(69.1)  302.9 G(73.8)       74   553188   931
    2016-06-26 01:35:24.984 4379071.992 UDP                  17    7.1 M(55.7)  138.0 M(29.3)  102.7 G(25.0)       31   187604   744
    2016-08-15 01:27:54.168 13106.624 GRE                  47      440( 0.0)    6.3 M( 1.3)    5.0 G( 1.2)      476    3.0 M   799

### Úkol 2, Syslog
1. Uveďte pravidlo pro přeposílání všech syslog zpráv na **cv5-master**: *.*    @192.168.56.102:514
2. Uveďte pravidlo, které omezí zprávy přeposílané z **cv5-provider** na zprávy týkající se pouze autentizace: authpriv.info   @192.168.56.102:514
3. Jakou zprávu odeslal **cv5-provider** při neúspěšném přihlášení? Stačí uvést pouze zkráceně: AUTHPRIV.INFO: Nov 27 14:17:06 localhost sshd[5637]: Failed password for root from 192.168.56.102 port 41776 ssh2

### Úkol 3, Icinga
1. Jaký je časový interval mezi kontrolami pro HTTP službu?: 9m 42s
2. Uveďte konfiguraci komunity pro SNMP ze stroje **cv5-provider**: rocommunity xkuder04    192.168.56.0/24
3. Uveďte množství volné paměti zaslané ve zprávě SNMP: 84780